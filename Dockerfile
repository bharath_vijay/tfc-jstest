#Base image
FROM node:argon

#Maintainer
MAINTAINER Silenced(bharathvijay38@gmail.com)

#Creating a working directory
RUN mkdir -p /usr/share/tfc-test
COPY . /usr/share/tfc-test
WORKDIR /usr/share/tfc-test/server

#Installing dependencies 
#COPY server/package.json /usr/share/tfc-test/server
RUN npm install
RUN npm install -g bower --allow-root
RUN bower install --allow-root

#Bundle app source


EXPOSE 8000

CMD [ "npm", "start" ]