var chai = require('chai'),
	expect = chai.expect,
	assert = chai.assert;
var gc = require('../lib/globalCounter');
var g_count = gc.globalCounter(4, 'global-counter', 45, 123);

describe('Global counter seems to work fine', function () {
	
	it('Works fine', function (done) {
		expect(g_count.output['Output']).to.be.a("number");		
		assert.equal(168, g_count.output["Output"], "Seems the value is correct");
		done();
	});

});
