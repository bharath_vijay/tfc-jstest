var expect = require('chai').expect,
	assert = require('chai').assert ;
var count = require("../lib/counter");
var counts = count.counter(2, 'counter', 56);

	describe("Returns the counter value", function() {

	it('It meets the requirements', function (done) {
		expect(counts.type).to.equal('counter');
		expect(parseInt(counts.output['Output'])).to.be.an('number');
		done();
	});

	it('Returns the correct output', function(done) {
		assert.equal(counts.output['Output'], 57, "Processed output is correct");
		done();
	});

	it('Do not process data less than 0', function(done) {
		assert.isAbove(parseInt(counts.output['Input']), 0, "Input is no greater than 0")
		done();
	});

});