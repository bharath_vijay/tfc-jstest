var chai = require('chai'),
	expect = chai.expect,
	assert = chai.assert,
	should = chai.should();
var hash = require('../lib/hash');
var hasher = hash.hasher(6, "hash", "cool")
var md5 = require('md5');

describe('MD5 hash the given string', function () {
	
	it("Takes the proper input", function(done){
		expect(hasher.output['Input']).be.a("string");
		done();
	});

	it("process a data", function(done){
		assert.equal(hasher.output['Output'], md5("cool"), "Returns the correct output");
		done();
	});

});